/*
SQLyog Community v13.1.6 (64 bit)
MySQL - 10.4.28-MariaDB : Database - database
*********************************************************************
*/

/*!40101 SET NAMES utf8 */;

/*!40101 SET SQL_MODE=''*/;

/*!40014 SET @OLD_UNIQUE_CHECKS=@@UNIQUE_CHECKS, UNIQUE_CHECKS=0 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;
/*!40111 SET @OLD_SQL_NOTES=@@SQL_NOTES, SQL_NOTES=0 */;
CREATE DATABASE /*!32312 IF NOT EXISTS*/`database` /*!40100 DEFAULT CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci */;

USE `database`;

/*Table structure for table `barang` */

DROP TABLE IF EXISTS `barang`;

CREATE TABLE `barang` (
  `kode_barang` int(11) NOT NULL,
  `nama_barang` varchar(50) DEFAULT NULL,
  `jumlah` char(11) DEFAULT NULL,
  PRIMARY KEY (`kode_barang`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_general_ci;

/*Data for the table `barang` */

insert  into `barang`(`kode_barang`,`nama_barang`,`jumlah`) values 
(1,'kamera','2'),
(2,'proyektor','5'),
(3,'kursi','1');

/*Table structure for table `mahasiswa` */

DROP TABLE IF EXISTS `mahasiswa`;

CREATE TABLE `mahasiswa` (
  `mhsNpm` char(12) NOT NULL,
  `mhsNama` varchar(255) DEFAULT NULL,
  `mhsAlamat` text DEFAULT NULL,
  `mhsFakultas` varchar(255) DEFAULT NULL,
  `mhsProdi` varchar(255) DEFAULT NULL,
  PRIMARY KEY (`mhsNpm`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_general_ci;

/*Data for the table `mahasiswa` */

insert  into `mahasiswa`(`mhsNpm`,`mhsNama`,`mhsAlamat`,`mhsFakultas`,`mhsProdi`) values 
('20311058','Urip','Sukarame','FTIK','SI'),
('20311420','Bima','Way Kandis','FTIK','SI'),
('20311435','Andra','Kemiling','FTIK','SI'),
('20311439','Juju','Rajabasa','FEB','Manajemen'),
('20311446','Abizar','Sukarame','FTIK','SI');

/*Table structure for table `peminjaman` */

DROP TABLE IF EXISTS `peminjaman`;

CREATE TABLE `peminjaman` (
  `npm_peminjam` int(11) NOT NULL,
  `kode_pengajuan` char(11) NOT NULL,
  `tanggal` date DEFAULT NULL,
  `nama_peminjam` varchar(255) DEFAULT NULL,
  `prodi` varchar(255) DEFAULT NULL,
  PRIMARY KEY (`kode_pengajuan`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_general_ci;

/*Data for the table `peminjaman` */

insert  into `peminjaman`(`npm_peminjam`,`kode_pengajuan`,`tanggal`,`nama_peminjam`,`prodi`) values 
(20311057,'A0001','2023-05-06','Dhyo Haw','Teknik Informatika'),
(20311058,'A0002','2023-05-06','urip hadi','sistem informasi'),
(20311059,'A0003','2023-05-06','afriyando','teknik sipil');

/*Table structure for table `peminjaman_detail` */

DROP TABLE IF EXISTS `peminjaman_detail`;

CREATE TABLE `peminjaman_detail` (
  `kode_pengajuan` char(11) NOT NULL,
  `kode_barang` varchar(50) DEFAULT NULL,
  `jumlah` char(11) DEFAULT NULL,
  PRIMARY KEY (`kode_pengajuan`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_general_ci;

/*Data for the table `peminjaman_detail` */

insert  into `peminjaman_detail`(`kode_pengajuan`,`kode_barang`,`jumlah`) values 
('006','0011','2'),
('A0001','0011','2'),
('A0002','0022','8'),
('A003','0022','2');

/*Table structure for table `pengembalian` */

DROP TABLE IF EXISTS `pengembalian`;

CREATE TABLE `pengembalian` (
  `kode_pengembalian` int(11) NOT NULL AUTO_INCREMENT,
  `kode_pengajuan` varchar(50) DEFAULT NULL,
  `tanggal_kembali` date DEFAULT NULL,
  PRIMARY KEY (`kode_pengembalian`)
) ENGINE=InnoDB AUTO_INCREMENT=22222223 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_general_ci;

/*Data for the table `pengembalian` */

insert  into `pengembalian`(`kode_pengembalian`,`kode_pengajuan`,`tanggal_kembali`) values 
(1,'A0001','2023-05-06'),
(2,'A0002','2023-05-06'),
(3,'A0003','2023-06-06');

/*Table structure for table `pengembalian_detail` */

DROP TABLE IF EXISTS `pengembalian_detail`;

CREATE TABLE `pengembalian_detail` (
  `kode_pengembalian_detail` int(11) NOT NULL AUTO_INCREMENT,
  `kode_pengembalian` varchar(50) DEFAULT NULL,
  `kode_barang` varchar(50) DEFAULT NULL,
  `jumlah` char(11) DEFAULT NULL,
  PRIMARY KEY (`kode_pengembalian_detail`)
) ENGINE=InnoDB AUTO_INCREMENT=3334 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_general_ci;

/*Data for the table `pengembalian_detail` */

insert  into `pengembalian_detail`(`kode_pengembalian_detail`,`kode_pengembalian`,`kode_barang`,`jumlah`) values 
(1111,'0001','0001','1'),
(2222,'0002','0002','3'),
(3333,'0003','0003','1');

/*Table structure for table `user` */

DROP TABLE IF EXISTS `user`;

CREATE TABLE `user` (
  `id_user` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(50) DEFAULT NULL,
  `username` varchar(50) DEFAULT NULL,
  `pass` varchar(255) DEFAULT NULL,
  `role` varchar(20) DEFAULT NULL,
  PRIMARY KEY (`id_user`)
) ENGINE=InnoDB AUTO_INCREMENT=4 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_general_ci;

/*Data for the table `user` */

insert  into `user`(`id_user`,`name`,`username`,`pass`,`role`) values 
(1,'andra','tes','28b662d883b6d76fd96e4ddc5e9ba780','admin'),
(3,'alfariz','test','202cb962ac59075b964b07152d234b70','Programmer');

/*!40101 SET SQL_MODE=@OLD_SQL_MODE */;
/*!40014 SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS */;
/*!40014 SET UNIQUE_CHECKS=@OLD_UNIQUE_CHECKS */;
/*!40111 SET SQL_NOTES=@OLD_SQL_NOTES */;
